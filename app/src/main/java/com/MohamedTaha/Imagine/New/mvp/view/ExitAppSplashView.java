package com.MohamedTaha.Imagine.New.mvp.view;

public interface ExitAppSplashView {
    void exitApp();

    void showMessageExitApp();
}
