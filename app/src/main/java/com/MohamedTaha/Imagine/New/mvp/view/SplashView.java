package com.MohamedTaha.Imagine.New.mvp.view;

public interface SplashView {
    void showAnimation();
    void goToMainActivity();
    void goToSlider();
}
